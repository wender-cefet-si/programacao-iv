package br.cefet.prog4;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Collection;

public class Main {

	public static void main(String[] args) throws ColecaoException, SQLException {
		String url = "jdbc:mysql://127.0.0.1/acme";
		Connection conexao = DriverManager.getConnection( url, "root", "" );
		ColecaoDeContatos colecao = new ColecaoDeContatosEmBDR( conexao );
		Collection< Contato > contatos = colecao.todos();
		for ( Contato c : contatos ) {
			System.out.println( c );
		}
		System.out.println( "Procurando com nome: ");
		System.out.println( colecao.comNome( "Bob Dylan" ) );
		System.out.println( "Procurando com telefonee: ");
		System.out.println( colecao.comTelefone( "33334444" ) );
		System.out.println( "Procurando com Id: ");
		System.out.println( colecao.comId( 4 ) );
		System.out.println( "Removendo com Id 6: ");
		colecao.remover( colecao.comId( 6 ) );
		System.out.println( "Com Id 6: ");
		System.out.println( colecao.comId( 6 ) );
	}

}
