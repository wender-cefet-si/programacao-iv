package br.cefet.prog4;

public class ColecaoException extends Exception {

	private static final long serialVersionUID = 5605240514849981564L;

	public ColecaoException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ColecaoException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super( message, cause, enableSuppression, writableStackTrace );
		// TODO Auto-generated constructor stub
	}

	public ColecaoException(String message, Throwable cause) {
		super( message, cause );
		// TODO Auto-generated constructor stub
	}

	public ColecaoException(String message) {
		super( message );
		// TODO Auto-generated constructor stub
	}

	public ColecaoException(Throwable cause) {
		super( cause );
		// TODO Auto-generated constructor stub
	}

}
