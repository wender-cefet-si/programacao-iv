package br.cefet.prog4;

/**
 * Contato
 *
 * @author Thiago Delgado Pinto
 * @see http://gihub.com/thiagodp
 */
public class Contato {

	private int id;
	private String nome;
	private String telefone;

	/**
	 * Cria um contato.
	 *
	 * @param id Identificação do usuário.
	 * @param nome
	 * @param telefone
	 */
	public Contato(int id, String nome, String telefone) {
		this.id = id;
		this.nome = nome;
		this.telefone = telefone;
	}

	public int getId() { return this.id; }
	public String getNome() { return this.nome; }
	public String getTelefone() { return this.telefone; }

	public void setId(int id) {
		this.id = id;
	}

	public void setNome(String snome) {
		this.nome = snome;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String toString() {
		return String.format( "{ \"id\": %d, \"nome\": \"%s\", \"telefone\": \"%s\" }",
				id, nome, telefone );
	}
}










