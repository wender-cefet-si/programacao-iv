package br.cefet.prog4.teste;

import static org.junit.Assert.assertNotNull;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.junit.Test;

import br.cefet.prog4.ColecaoDeContatos;
import br.cefet.prog4.ColecaoDeContatosEmBDR;
import br.cefet.prog4.ColecaoException;

public class TesteColecaoDeContatosEmBDR {

	private ColecaoDeContatos colecao;

	public  TesteColecaoDeContatosEmBDR() throws SQLException {
		String url = "jdbc:mysql://127.0.0.1/acme";
		Connection conexao = DriverManager.getConnection( url, "root", "" );
		colecao = new ColecaoDeContatosEmBDR( conexao );
	}

	@Test
	public void consegueEncontrarUmContatoPeloNome() throws ColecaoException {
		assertNotNull( colecao.comNome( "Bob Dylan" ) );
	}

	@Test
	public void consegueEncontrarUmContatoPeloId() throws ColecaoException {
		assertNotNull( colecao.comId( 2 ) );
	}

}
