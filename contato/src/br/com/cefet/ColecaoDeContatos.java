package br.cefet.prog4;

import java.util.Collection;

public interface ColecaoDeContatos {

	void adicionar(Contato contato) throws ColecaoException;

	void atualizar(Contato contato) throws ColecaoException;

	void remover(Contato contato) throws ColecaoException;

	Collection< Contato > todos() throws ColecaoException;

	Contato comNome(final String nome) throws ColecaoException;

	Contato comTelefone(final String nome) throws ColecaoException;

	Contato comId(final int id) throws ColecaoException;

}
