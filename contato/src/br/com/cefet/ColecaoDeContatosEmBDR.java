package br.cefet.prog4;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ColecaoDeContatosEmBDR implements ColecaoDeContatos {

	private final Connection conexao;

	public ColecaoDeContatosEmBDR(Connection c) {
		this.conexao = c;
	}

	public void adicionar(Contato contato) throws ColecaoException {
		String sql = "INSERT INTO contato ( nome, telefone )  " +
				" VALUES ( ?, ? )";
		try {
			// Executa o comando
			PreparedStatement ps = conexao.prepareStatement( sql );
			ps.setString( 1, contato.getNome() );
			ps.setString( 2, contato.getTelefone() );
			ps.execute();

			// Obtém o id geradi
			ResultSet rs = ps.getGeneratedKeys();
			rs.next();
			int id = rs.getInt( 1 );

			// Seta o id gerado no contato
			contato.setId( id );

		} catch (SQLException e) {
			String message = "Erro ao adicionar o contato";
			throw new ColecaoException( message, e );
		}
	}

	@Override
	public void atualizar(Contato contato) throws ColecaoException {
		String sql = "UPDATE contato SET nome = ?, telefone = ?  " +
				" WHERE id = ?";
		try {
			PreparedStatement ps = conexao.prepareStatement( sql );
			ps.setString( 1, contato.getNome() );
			ps.setString( 2, contato.getTelefone() );
			ps.setInt( 3, contato.getId() );
			ps.execute();
		} catch (SQLException e) {
			String message = "Erro ao atualizar o contato";
			throw new ColecaoException( message, e );
		}
	}

	@Override
	public void remover(Contato contato) throws ColecaoException {
		if ( null == contato ) {
			return;
		}
		String sql = "DELETE FROM contato WHERE id = ?";
		try {
			PreparedStatement ps = conexao.prepareStatement( sql );
			ps.setInt( 1, contato.getId() );
			ps.execute();
		} catch (SQLException e) {
			String message = "Erro ao remover o contato";
			throw new ColecaoException( message, e );
		}
	}

	@Override
	public Collection< Contato > todos() throws ColecaoException {
		Map< String, Object > filtros = new HashMap< String, Object >();
		return contatosCom( filtros );
	}

	@Override
	public Contato comNome(final String nome) throws ColecaoException {
		Map< String, Object > filtros = new HashMap< String, Object >();
		filtros.put( "nome", nome );
		List< Contato > contatos = contatosCom( filtros );
		return contatos.isEmpty() ? null : contatos.get( 0 );
	}

	@Override
	public Contato comTelefone(final String telefone) throws ColecaoException {
		Map< String, Object > filtros = new HashMap< String, Object >();
		filtros.put( "telefone", telefone );
		List< Contato > contatos = contatosCom( filtros );
		return contatos.isEmpty() ? null : contatos.get( 0 );
	}

	@Override
	public Contato comId(int id) throws ColecaoException {
		Map< String, Object > filtros = new HashMap< String, Object >();
		filtros.put( "id", id );
		List< Contato > contatos = contatosCom( filtros );
		return contatos.isEmpty() ? null : contatos.get( 0 );
	}


	private List< Contato > contatosCom(
			final Map< String, Object> filtros
			) throws ColecaoException {
		List< Contato > contatos = new ArrayList< Contato >();
		try {
			ResultSet rs = executarComFiltros( filtros );
			while ( rs.next() ) {
				Contato c = new Contato(
						rs.getInt( 1 ),
						rs.getString( 2 ),
						rs.getString( 3 )
						);
				contatos.add( c );
			}
		} catch (SQLException e) {
			String message = "Erro ao consultar os contatos";
			throw new ColecaoException( message, e );
		}
		return contatos;
	}

	private ResultSet executarComFiltros(final Map< String, Object > filtros) throws SQLException {
		final String POSSIVEIS[] = { "id", "nome", "telefone" };
		List< String > campos = new ArrayList< String >();
		String where = "";
		for ( String p : POSSIVEIS ) {
			if ( filtros.get( p ) != null ) {
				campos.add( p );
				if ( where.isEmpty() ) {
					where += " WHERE ";
				} else {
					where += " AND ";
				}
				where += p  + " = ?";
			}
		}
		String sql = "SELECT id, nome, telefone FROM contato" ;
		if ( ! where.isEmpty() ) {
			sql += where;
		}
		PreparedStatement ps = conexao.prepareStatement( sql );
		int i = 1;
		for ( String c : campos ) {
			ps.setObject( i, filtros.get( c  ) );
			++i;
		}
		return ps.executeQuery();
	}

}
